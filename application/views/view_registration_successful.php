<?php
require_once("header.php");
require_once("menu_guest.php");
?>

<!-- .container -->
<div class="container" >
    <div class="row">
        <div class="box" style="height: 600px; overflow-y: scroll;">
            <p style="font-size: 30px; text-align: center; margin-top: 30px;">Регистрация прошла успешно!</p>
            <p style="font-size: 30px; text-align: center;">Войдите в систему</p>
        </div>
    </div>
</div>

<?php require_once("footer.php"); ?>