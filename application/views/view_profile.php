<? require_once("header.php"); ?>

    <? require_once("menu_auth.php"); ?>

    <!-- .container -->
    <div class="container">
        <div class="row">
            <div class="box">
                <div class="col-md-12 col-lg-12">
                    <hr>
                    <h2 class="intro-text text-center">Мой профиль</h2>
                    <hr>
                </div>
                <div class="col-md-5" style="margin: 20px 0px 20px 120px; text-align: center; max-height: 500px; max-width: 400px;">
                    <img class="img-responsive img-border-left" src="/img/avatar.jpg" alt="">
                </div>
                <div class="col-md-4" style="margin: 20px 0px 20px 110px; text-align: left;">
                    <p><strong>Имя: </strong>Анастасия</p><br>
                    <p><strong>Фамилия: </strong>Сухарева</p><br>
                    <p><strong>Пол: </strong>женский</p><br>
                    <p><strong>Дата рождения: </strong>29.11.1995</p><br>
                    <p><strong>E-mail: </strong>Nastya199529@mail.ru</p><br>
                    <p><strong>Логин: </strong>Nastya</p><br>
                    <button type="submit" class="btn btn-default" style=" margin: 125px 0px 0px; width: 200px; height: 50px">Редактировать</button>
                </div>

                <div class="clearfix"></div>
                <br>
            </div>
        </div>
    </div>
    <!-- /.container -->

<? require_once("footer.php"); ?>

</html>
