<?php
require_once("header.php");
require_once("menu_auth.php");
?>

<!-- .container -->
<div class="container" >
    <div class="row">
        <div class="box" style="height: 600px; overflow-y: scroll;">

            <div class="col-md-12 col-lg-12">
                <hr>
                <h2 class="intro-text text-center">Все мероприятия</h2>
                <hr>
            </div>
            <div class="col-md-10" style="margin: 20px 0px 30px 150px;">
                <p>Найти в период c <input type="date" name="from"> по <input type="date" name="to">
                    <input type="submit" class="btn btn-default" style="margin-left: 50px; width: 200px; height: 40px" value="Поиск">
                </p>
            </div>

            <!-- Мероприятие 1 -->
            <div class="main" style="outline: 1px solid #cc78a9; width:1000px;  margin: 20px 0px 30px 60px; overflow:hidden;">
                <div class=" photo" style="; width:240px; float:left; padding-bottom:32000px; margin-bottom:-32000px;">
                    <p style="text-align: center; margin-top: 20px;"><img src="/img/elderly.jpg" style="max-height: 200px; max-width:200px;"></p>
                </div>
                <div class="description" style=" width:760px; float:left; padding-bottom:32000px; margin-bottom:-32000px;">
                    <div style="margin: 20px 10px 10px 10px;">
                        <p><strong>Название мероприятия:</strong> Волонтёр для участия в концерте в доме престарелых</p>
                        <p><strong>Дата:</strong> 15.09.2017</p>
                        <p><strong>Место проведения:</strong> г.Ульяновск</p>
                        <p style="white-space: nowrap; overflow: hidden; text-overflow: ellipsis;"><strong>Описание:</strong> В наших поездках в дома престарелых мы общаемся с бабушками, поем с ними песни прошлых лет, играем в простые игры.
                            От волонтера нужна любовь и уважение к пожилым людям, а также желание помогать в команде: грузить подгузники, конфеты, петь песни с группой, общаться с проживающими.
                            Важно: если до места группа едет на поезде, билеты и еду волонтер оплачивает самостоятельно. Это ваш вклад в заботу о пожилых, у которых мало такой заботы и внимания.
                            Если едем на машинах, возможно будем скидываться на бензин водителю. Хотя бывает, что водитель ничего не берет с группы.</p>
                    </div>
                </div>
                <div class="status" style="clear:both; float:left; width:100%;">
                    <input type="submit" value="Хочу участвовать!" class="btn btn-default" style="margin: 10px 10px 20px 45px;">
                    <input type="submit" value="Написать организатору" class="btn btn-default" style="margin: 10px 10px 20px 40px;">
                    <input type="submit" value="Открыть в полном размере" class="btn btn-default" style="text-align: left; margin: 10px 10px 20px 310px;">
                </div>
            </div>

            <!-- Мероприятие 2 -->
            <div class="main" style="outline: 1px solid #cc78a9; width:1000px;  margin: 20px 0px 30px 60px; overflow:hidden;">
                <div class=" photo" style="; width:240px; float:left; padding-bottom:32000px; margin-bottom:-32000px;">
                    <p style="text-align: center; margin-top: 20px;"><img src="/img/dogs.jpg" style="max-height: 200px; max-width:200px;"></p>
                </div>
                <div class="description" style=" width:760px; float:left; padding-bottom:32000px; margin-bottom:-32000px;">
                    <div style="margin: 20px 10px 10px 10px;">
                        <p><strong>Название мероприятия:</strong> Выгул бездомных животных в приюте "Друг"</p>
                        <p><strong>Дата:</strong> 13.06.2017</p>
                        <p><strong>Место проведения:</strong> г.Ульяновск</p>
                        <p style="white-space: nowrap; overflow: hidden; text-overflow: ellipsis;"><strong>Описание:</strong> Друзья, приезжайте в #ПриютДруг , гуляйте с хвостиками, общайтесь с ними, и возможно, что вы даже найдете нового ДРУГА!
                            Мы с волонтёрами #ДаДобро, с нетерпением ждём следующих выходных!  Мы навещаем приют каждую субботу, встречаемся на ж/д станции Ржевке. Все вопросы можно зада вк  vk.com/yutaya или по телефону 8(904)5530354.  Хотите с нами?) Так скорее же присоединяйтесь к СИЛАМ ДОБРА!😉  </p>
                    </div>
                </div>
                <div class="status" style="clear:both; float:left; width:100%;">
                    <div style="outline: 1px solid #e4e4e4; background: linear-gradient(to top, #e8e6c6, white); width:153px; height: 40px; margin: 10px 10px 20px 45px; text-align: center; float: left;">Ожидается подтверждение</div>
                    <input type="submit" value="Написать организатору" class="btn btn-default" style="margin: 13px 10px 20px 40px; float: left;">
                    <input type="submit" value="Открыть в полном размере" class="btn btn-default" style="text-align: left; margin: 13px 10px 20px 310px;">
                </div>
            </div>

            <!-- Мероприятие 3 -->
            <div class="main" style="outline: 1px solid #cc78a9; width:1000px;  margin: 20px 0px 30px 60px; overflow:hidden;">
                <div class=" photo" style=" width:240px; float:left; padding-bottom:32000px; margin-bottom:-32000px;">
                    <p style="text-align: center; margin-top: 20px;"><img src="/img/social.jpg" style="max-height: 200px; max-width:200px;"></p>
                </div>
                <div class="description" style=" width:760px; float:left; padding-bottom:32000px; margin-bottom:-32000px;">
                    <div style="margin: 20px 10px 10px 10px;">
                        <p><strong>Название мероприятия:</strong> Волонтёр для соц.сетей</p>
                        <p><strong>Дата:</strong> 01.03.2017</p>
                        <p><strong>Место проведения:</strong> г.Ульяновск</p>
                        <p style="white-space: nowrap; overflow: hidden; text-overflow: ellipsis;"><strong>Описание:</strong>Волонтер нужен в феврале! Ищем людей, которые помогали бы фонду с продвижением информации о сборе средств на лагерь на краудфандинговой платформе Planeta.ru.
                            Можно писать группам ВКонтакте о перепостах, писать в Инстаграме, Фейсбуке. Вы также можете помочь фонду  с распространением информации о нас людям с большим количеством подписчиков,  или просто своим друзьям и знакомым. Помощь нужна сейчас. </p>
                    </div>
                </div>
                <div class="status" style="clear:both; float:left; width:100%;">
                    <div style="outline: 1px solid #e4e4e4;  background: linear-gradient(to top, #7ec6a5, white); width:153px; height: 40px; margin: 10px 10px 20px 45px; text-align: center; line-height: 35px; float: left;">Я - волонтёр!</div>
                    <input type="submit" value="Написать организатору" class="btn btn-default" style="margin: 13px 10px 20px 40px; float: left;">
                    <input type="submit" value="Открыть в полном размере" class="btn btn-default" style="text-align: left; margin: 13px 10px 20px 310px;">
                </div>
            </div>

        </div>
    </div>
</div>

<?php
require_once("footer.php");
?>
