<!-- Navigation -->
<nav class="navbar navbar-default" role="navigation">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <!-- navbar-brand is hidden on larger screens, but visible when the menu is collapsed -->
            <a class="navbar-brand" href="/application/views/view_profile.php">Волонтёр</a>
        </div>
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav menu">
                <li style="margin: 0px 410px 0px 0px"><a href="/application/views/view_events_for_guest.php">Мероприятия</a></li>
                <li>
                    <a href="/application/views/view_FAQ.php">FAQ</a>
                </li>
                <li>
                    <a href="/application/views/view_?__.php">Связаться с нами</a>
                </li>
                <li>
                    <a href="/application/views/view_avtorization.php">Войти</a>
                </li>
            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </div>
    <!-- /.container -->
</nav>